package policyrepository;

import org.apache.commons.beanutils.BeanUtils;

import com.google.gson.Gson;
import com.jmmonen.policyrepository.domain.person.model.Person;
import com.jmmonen.policyrepository.domain.person.model.PersonDto;
import com.jmmonen.policyrepository.utilities.DateAdapter;

public class Main {

	public static void main(String[] args) throws Exception {
		
		
		PersonDto personDto = new  PersonDto();
//		personDto.setId("09644c7e-60f8-4a77-ac51-9a9018421209");
		personDto.setFirstName("Jaska");
		personDto.setLastName("Jokunen");
		personDto.setDateOfBirth(new DateAdapter().unmarshal("2000-01-01"));
		
		Person person = null;
		
		if(personDto.getId() != null)
			person = new Person(personDto.getId());
		else
			person = new Person();
		
		BeanUtils.copyProperties(person, personDto);
		
		
		System.out.println(new Gson().toJson(personDto));
		System.out.println("---------->>>>>>>>>");
		
		System.out.println(new Gson().toJson(person));
		
		
	}

}
