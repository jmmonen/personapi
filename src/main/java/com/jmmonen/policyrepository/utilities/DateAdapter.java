/**
 * 
 */
package com.jmmonen.policyrepository.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * @author 2019 - jmmonen
 *
 */
public class DateAdapter extends XmlAdapter<String, Date> {

	@Override
	public Date unmarshal(String $value) throws Exception {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.parse($value);
	}

	@Override
	public String marshal(Date $value) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format($value);
	}

}
