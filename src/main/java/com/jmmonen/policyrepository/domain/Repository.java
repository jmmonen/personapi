/**
 * 
 */
package com.jmmonen.policyrepository.domain;

import com.jmmonen.policyrepository.domain.persistence.model.Identifiable;

/**
 * @author 2019 - jmmonen
 *
 */
public interface Repository<T extends Identifiable> {
	
	Class<T> getType();
	
	T store(T $entity);
	
	T get(String $id);
	
	void remove(T $entity);

}
