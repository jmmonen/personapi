/**
 * 
 */
package com.jmmonen.policyrepository.domain.person.model;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.jmmonen.policyrepository.domain.persistence.model.BaseEntity;

/**
 * @author 2019 - jmmonen
 *
 */

@Entity
@Audited
public class Address extends BaseEntity{

	private static final long serialVersionUID = 1L;
	
	private String streetAddress;
	
	private String postalCode;
	
	private String city;
	
	@ManyToOne
	@JoinColumn(name = "person_id")
	@JsonBackReference
	private Person person;

	// ---------- Constructors

	
	public Address(
			String $streetAddress, 
			String $postalCode, 
			String $city) {
		
		super(UUID.randomUUID().toString());
		this.streetAddress = $streetAddress;
		this.postalCode = $postalCode;
		this.city = $city;
	}

	protected Address() {
		
		super(UUID.randomUUID().toString());
	}

	// ---------- Getters and Setters
	

	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}

	/**
	 * @param streetAddress
	 *            the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @param person the person to set
	 */
	public void setPerson(Person person) {
		this.person = person;
	}
	
	

}
