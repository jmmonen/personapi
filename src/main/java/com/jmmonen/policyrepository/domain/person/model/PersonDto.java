/**
 * 
 */
package com.jmmonen.policyrepository.domain.person.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.jmmonen.policyrepository.utilities.DateAdapter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author 2019 - jmmonen
 *
 */
@ApiModel(value="Person model", description="A model of Person entity")
public class PersonDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "An id of a person entity when updating an existing record", example = "de99544c-43ca-479a-869b-1381cc0c095a", required = false)
	private String id;

	@ApiModelProperty(value = "Person first name", example = "John", required = true)
	private String firstName;

	@ApiModelProperty(value = "Person last name", example = "Doe", required = true)
	private String lastName;

	@ApiModelProperty(value = "Person date of birth", example = "1980-10-01", required = true)
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date dateOfBirth;
	
	
	// ---------- Getters and Setters
	

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

}
