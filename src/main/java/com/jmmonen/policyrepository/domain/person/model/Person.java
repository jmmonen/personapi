/**
 * 
 */
package com.jmmonen.policyrepository.domain.person.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.hibernate.envers.Audited;

import com.jmmonen.policyrepository.domain.persistence.model.BaseEntity;
import com.jmmonen.policyrepository.utilities.DateAdapter;


/**
 * @author 2019 - jmmonen
 *
 */

@Entity
@Audited
public class Person extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String firstName;
	
	private String lastName;
	
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date dateOfBirth;

	// ---------- Constructors

	
	public Person(
			String $firstName, 
			String $lastName, 
			String $dateOfBirth)
			throws Exception {

		super(UUID.randomUUID().toString());
		
		this.firstName = $firstName;
		this.lastName = $lastName;
		this.dateOfBirth = new DateAdapter().unmarshal($dateOfBirth);

	}
	
	public Person(String $id) {
		
		super($id);
	}

	public Person() {
		
		super(UUID.randomUUID().toString());
	}

	// ---------- Getters and Setters

	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

}
