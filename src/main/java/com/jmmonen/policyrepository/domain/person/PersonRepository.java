/**
 * 
 */
package com.jmmonen.policyrepository.domain.person;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Typed;
import javax.persistence.TypedQuery;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;

import com.jmmonen.policyrepository.domain.persistence.PersistenceRepository;
import com.jmmonen.policyrepository.domain.person.model.Person;

/**
 * @author 2019 - jmmonen
 *
 */
@Stateless
@LocalBean
@Typed(PersonRepository.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PersonRepository extends PersistenceRepository<Person> {

	public PersonRepository() {

		super(Person.class);
	}

	
	/**
	 * 
	 * 
	 * @param $startPosition
	 * @param $maxResult
	 * @return
	 */
	public List<Person> listAll(Integer $startPosition, Integer $maxResult) {

		TypedQuery<Person> findAllQuery = this.getManager().createQuery("FROM Person ORDER BY created desc", Person.class);

		if ($startPosition != null) {
			findAllQuery.setFirstResult($startPosition);
		}
		if ($maxResult != null) {
			findAllQuery.setMaxResults($maxResult);
		}

		final List<Person> results = findAllQuery.getResultList();

		return results;
	}
	
	
	/**
	 * Get revision of the Person entity
	 * 
	 * @param $personId
	 * @return The result
	 */
	public List<Person> getRevision(String $personId) {

		AuditReader auditReader = AuditReaderFactory.get(this.getManager());
		List<Number> revisionNumbers = auditReader.getRevisions(Person.class, $personId);
		
		List<Person> results = new ArrayList<Person>();

		for (Number rev : revisionNumbers) {
			
			results.add((Person) auditReader.find(Person.class, $personId, rev));
		}

		return results;
	}

}