/**
 * 
 */
package com.jmmonen.policyrepository.domain.person;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Typed;
import javax.persistence.TypedQuery;

import com.jmmonen.policyrepository.domain.persistence.PersistenceRepository;
import com.jmmonen.policyrepository.domain.person.model.Address;

/**
 * @author 2019 - jmmonen
 *
 */
@Stateless
@LocalBean
@Typed(AddressRepository.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AddressRepository extends PersistenceRepository<Address> {

	public AddressRepository() {

		super(Address.class);
	}

	public List<Address> getAddressList(String $personId, Integer $startPosition, Integer $maxResult) {

		TypedQuery<Address> getAddressListQuery = this.getManager().createQuery("FROM Address WHERE person_id = :personId ORDER BY created desc", Address.class);
		getAddressListQuery.setParameter("personId", $personId);

		if ($startPosition != null) {
			getAddressListQuery.setFirstResult($startPosition);
		}
		if ($maxResult != null) {
			getAddressListQuery.setMaxResults($maxResult);
		}

		final List<Address> results = getAddressListQuery.getResultList();

		return results;
	}
	
	
//	public List<Person> getRevision(String $personId) {
//
//		AuditReader auditReader = AuditReaderFactory.get(this.getManager());
//		List<Number> revisionNumbers = auditReader.getRevisions(Person.class, $personId);
//		
//		List<Person> results = new ArrayList<Person>();
//
//		for (Number rev : revisionNumbers) {
//			
//			results.add((Person) auditReader.find(Person.class, $personId, rev));
//		}
//
//		return results;
//	}

}