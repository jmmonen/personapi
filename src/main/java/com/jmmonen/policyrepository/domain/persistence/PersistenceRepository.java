/**
 * 
 */
package com.jmmonen.policyrepository.domain.persistence;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.jmmonen.policyrepository.domain.Repository;
import com.jmmonen.policyrepository.domain.persistence.model.Identifiable;

/**
 * @author 2019 - jmmonen
 *
 */
public abstract class PersistenceRepository<T extends Identifiable> implements Repository<T> {
	
	@PersistenceContext
	private EntityManager manager;
	
	private Class<T> type;
	
	
	// ---------- Constructors
	
	
	public PersistenceRepository(Class<T> $type) {
		
		this.type = $type;
	}
	
	
	// ---------- Getters and Setters
	

	/*
	 * (non-Javadoc)
	 * @see com.cgi.ir.domain.Repository#getType()
	 */
	@Override
	public Class<T> getType() {

		return type;
	}
	
	protected EntityManager getManager() {
		
		return manager;
	}
	
	
	// ---------- Functionality
	

	/*
	 * (non-Javadoc)
	 * @see com.cgi.ir.domain.Repository#store(com.cgi.ir.domain.persistence.model.Identifiable)
	 */
	@Override
	public T store(T $entity) {
		
		T merged = merge($entity);	
		manager.persist(merged);		
		manager.flush();
		
		return merged;
	}

	/*
	 * (non-Javadoc)
	 * @see com.jmmonen.policyrepository.domain.Repository#get(java.lang.String)
	 */
	@Override
	public T get(String $id) {
		
		return manager.find(type, $id);		
	}

	/*
	 * (non-Javadoc)
	 * @see com.cgi.ir.domain.Repository#remove(com.cgi.ir.domain.persistence.model.Identifiable)
	 */
	@Override
	public void remove(T $entity) {

		manager.remove(merge($entity));		
	}
	
	private T merge(T $entity) {
		
		return manager.merge($entity);
	}

}
