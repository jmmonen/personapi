/**
 * 
 */
package com.jmmonen.policyrepository.domain.persistence.model;

/**
 * @author 2019 - jmmonen
 *
 */
public interface Identifiable {
	
	String getId();

}
