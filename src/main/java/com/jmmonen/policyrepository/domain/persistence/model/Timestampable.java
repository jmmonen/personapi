/**
 * 
 */
package com.jmmonen.policyrepository.domain.persistence.model;

import java.util.Date;

/**
 * @author 2019 - jmmonen
 *
 */
public interface Timestampable {
	
	Date getCreated();
	
	Date getLastModified();

}
