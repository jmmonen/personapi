/**
 * 
 */
package com.jmmonen.policyrepository.domain.persistence.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author 2019 - jmmonen
 *
 */
@MappedSuperclass
public class BaseEntity implements Identifiable, Timestampable, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;

	// ---------- Constructors

	
	public BaseEntity(String $id) {
		this.id = $id;
	}

	protected BaseEntity() {

	}

	// ---------- Getters & Setters

	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jmmonen.policy.domain.persistence.model.Identifiable#getId()
	 */
	@Override
	public String getId() {

		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jmmonen.policy.domain.persistence.model.Timestampable#getCreated()
	 */
	@Override
	public Date getCreated() {

		return created == null ? null : (Date) created.clone();
	}

	public Date getLastUpdated() {

		return updated == null ? null : (Date) updated.clone();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jmmonen.policy.domain.persistence.model.Timestampable#getLastModified()
	 */
	@Override
	public Date getLastModified() {

		return getLastUpdated() == null ? getCreated() : getLastUpdated();

	}

	@PrePersist
	protected void onCreate() {
		
		created = new Date();
	}

	@PreUpdate
	protected void onUpdate() {
		
		updated = new Date();
	}

}
