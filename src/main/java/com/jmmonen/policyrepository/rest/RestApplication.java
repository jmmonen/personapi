/**
 * 
 */
package com.jmmonen.policyrepository.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author 2018 - Johannes Immonen
 *
 */
@ApplicationPath("/rest")
public class RestApplication extends Application {

}
