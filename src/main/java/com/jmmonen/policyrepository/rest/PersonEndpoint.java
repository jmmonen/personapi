/**
 * 
 */
package com.jmmonen.policyrepository.rest;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.beanutils.BeanUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmmonen.policyrepository.domain.person.AddressRepository;
import com.jmmonen.policyrepository.domain.person.PersonRepository;
import com.jmmonen.policyrepository.domain.person.model.Address;
import com.jmmonen.policyrepository.domain.person.model.Person;
import com.jmmonen.policyrepository.domain.person.model.PersonDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author 2018 - Johannes Immonen
 *
 */

@Stateless
@Api("person")
@Path("/person")
public class PersonEndpoint {

	@Inject
	private PersonRepository personRepository;
	
	@Inject
	private AddressRepository addressRepository;
	
	
	// ---------->>>>>>>>>> Person <<<<<<<<<<----------
	
	/**
	 * List all persons
	 * 
	 * @param startPosition
	 * @param maxResult
	 * @return
	 */

	@GET
	@Produces("application/json")
	@ApiOperation(
			value = "List persons",
			notes = "Returns a list of all persons")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok")
	})
	public Response listAll(
			@ApiParam(value = "Start position", required = false) @QueryParam("start") Integer startPosition, 
			@ApiParam(value = "Query maximum results", required = false) @QueryParam("max") Integer maxResult) {

		return Response
				.ok(personRepository.listAll(startPosition, maxResult))
				.build();
	}
	
	
	/**
	 * Get person by id
	 * 
	 * @param $id
	 * @return
	 */

	@GET
	@Path("/{id}")
	@Produces("application/json")
	@ApiOperation(
			value = "Get person by id",
			notes = "Returns a person record identified by unique id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 404, message = "No instance of person found by given id")
	})
	public Response getPersonbyId(
			@ApiParam(value = "Person unique id", required = true) @PathParam("id") String $id) {
		
		Map<String, String> message = new HashMap<String, String>();
		
		Person person = personRepository.get($id);
		if (person == null) {
			
			message.put("Exception", "id not found");
			return Response
					.status(Status.NOT_FOUND)
					.entity(message)
					.build();
		}

		return Response
				.ok(personRepository.get($id))
				.build();
	}
	
	
	/**
	 * Store or update a person
	 * 
	 * @param $payload
	 * @return
	 */

	@POST
	@Consumes("application/json")
	@Produces("application/json")
	@ApiOperation(
			value = "Store or update a person",
			notes = "Create new person record. If payload includes an existing person id, the existing record will be updated")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 400, message = "Malformed payload")
	})
	public Response storePerson(
			@ApiParam(name = "payload", value = "Content of person record", required = true)			
			PersonDto $payload) {
		
		Person person = null;
		
		// Inspect if the person id is given on payload
		if($payload.getId() != null)
			person = new Person($payload.getId());
		else
			person = new Person();
		
		
		try {
			BeanUtils.copyProperties(person, $payload);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Map<String, String> message = new HashMap<String, String>();

		Person merged = personRepository.store(person);
		
		message.put("id", merged.getId());
		message.put("name", merged.getFirstName());

		return Response
				.ok()
				.entity(message)
				.build();
	}
	
	// ---------->>>>>>>>>> Address <<<<<<<<<<----------
	
	/**
	 * List addresses
	 * 
	 * @param $id
	 * @param startPosition
	 * @param maxResult
	 * @return
	 */
	
	@GET
	@Path("/{id}/address")
	@Produces("application/json")
	@ApiOperation(
			value = "List addresses",
			notes = "Returns a list of addresses associated to the person identified by unique id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 404, message = "No instance of person found by given id")
	})
	public Response listAddresses(
			@ApiParam(value = "Person unique id", required = true) @PathParam("id") String $id,
			@ApiParam(value = "Start position", required = false) @QueryParam("start") Integer startPosition, 
			@ApiParam(value = "Query maximum results", required = false) @QueryParam("max") Integer maxResult) {

		return Response
				.ok(addressRepository.getAddressList($id, startPosition, maxResult))
				.build();
	}
	
	
	/**
	 * Store address
	 * 
	 * @param $id
	 * @param $payload
	 * @return
	 */	
	
	@POST
	@Path("/{id}/address")
	@Consumes("application/json")
	@Produces("application/json")
	@ApiOperation(
			value = "Store or update an address",
			notes = "Create new address record. If payload includes an existing address id, the existing record will be updated")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 400, message = "Malformed payload"),
			@ApiResponse(code = 404, message = "No instance of person found by given id")
	})
	public Response storeAddress(
			@PathParam("id") String $id,
			String $payload) {

		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> message = new HashMap<String, String>();
		
		Address address = null;
		
		Person person = personRepository.get($id);
		if (person == null) {
			
			message.put("Exception", "id not found");
			return Response
					.status(Status.NOT_FOUND)
					.entity(message)
					.build();
		}
			
		
		try {
			address = mapper.readValue($payload, Address.class);
			
		} catch (JsonParseException e) {
			e.printStackTrace();
			return Response
					.status(Status.BAD_REQUEST)
					.build();
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return Response
					.status(Status.BAD_REQUEST)
					.build();
			
		} catch (IOException e) {
			e.printStackTrace();
			return Response
					.status(Status.BAD_REQUEST)
					.build();
		}
		
		address.setPerson(person);	
		
		Address merged = addressRepository.store(address);
		
		message.put("id", merged.getId());
		
		return Response
				.ok()
				.entity(message)
				.build();
	}
	
	
	// ---------->>>>>>>>>> Person revision <<<<<<<<<<----------
	
	/**
	 * Get person revision
	 * 
	 * @param $id
	 * @return
	 */
	
	@GET
	@Path("/rev/{id}")
	@Produces("application/json")
	@ApiOperation(
			value = "Get person revision by id",
			notes = "Returns a list of person revision records identified by unique id")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok"),
			@ApiResponse(code = 404, message = "No instance of person found by given id")
	})
	public Response getPersonRevision(
			@PathParam("id") String $id) {
		
		return Response.
				ok(personRepository.getRevision($id))
				.build();
		
	}

}
